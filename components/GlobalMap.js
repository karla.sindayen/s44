import { useState, useRef, useEffect } from 'react';

import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

const GlobalMap = ({data}) => {
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [zoom, setZoom] = useState(0);
  const mapContainerRef = useRef(null)

  useEffect(() => {

    fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${data.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)

        setLongitude(data.features[0].center[0])
        setLatitude(data.features[0].center[1])
        setZoom(1)
      })

    const map = new mapboxgl.Map({
    
      container: mapContainerRef.current,
    
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [longitude, latitude],
      zoom: zoom
    })

    map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

    const marker = new mapboxgl.Marker()
    .setLngLat([longitude, latitude])
    .addTo(map)

    return () => map.remove()
  }, [latitude, longitude])


 return <div className="mapContainer" ref={mapContainerRef} />

}

export default GlobalMap