import { useState, useRef, useEffect } from 'react';	
import { Form, Button, Col, Row } from 'react-bootstrap'; 
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY


import toNum from '../../helpers/toNum'
import DoughnutChart from '../../components/DoughnutChart';

export default function Home({data}){
	console.log(data.countries_stat)

	const countriesStats = data.countries_stat;
	const [targetCountry, setTargetCountry] = useState('');
	const [name, setName] = useState('');
	const [criticals, setCriticals] = useState(0);
	const [deaths, setDeaths] = useState(0);
	const [recoveries, setRecoveries] = useState(0);

	const [latitude, setLatitude] = useState(0);
	const [longitude, setLongitude] = useState(0);
	const [zoom, setZoom] = useState(0);

	//use the useRef() hook to set a container where the map will be rendered
	const mapContainerRef = useRef(null)

	function search(e){
		e.preventDefault();

			const match = countriesStats.find(country => country.country_name.toLowerCase()
				=== targetCountry.toLowerCase())
			console.log(match)

			if(match){
			setName(match.country_name)
			setCriticals(toNum(match.serious_critical))
			setDeaths(toNum(match.deaths))
			setRecoveries(toNum(match.total_recovered))

			//use mapbox's geocpding API to return the coordicantes
			fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${targetCountry}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setLongitude(data.features[0].center[0])
				setLatitude(data.features[0].center[1])
				setZoom(1)
			})
	}
}

	useEffect(() => {
		//Instatitae  a new mapbox map object
		const map = new mapboxgl.Map({
			//set the container for the map as the current component
			container: mapContainerRef.current,
			//set the style  for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: zoom
		})

		//add navigation control or zoom in/out
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		//creates a marker centered on the provided longitude and latitude
		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		//clean up and release all resources associated with the map 
		return () => map.remove()
	}, [latitude, longitude])

	//div => [0, 1]
	//mapremove => [1] - remove the first element

	return(
		<React.Fragment>
			<Form onSubmit={e => search(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Search for country" 
						value={targetCountry} 
						onChange={e => setTargetCountry(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched for country.
					</Form.Text>
				</Form.Group>

				<Button className="primary" type="submit">
				Submit
				</Button>
				</Form>

			<h1>Country: {name} </h1>
			<Row>
				<Col xs={12} md={6}>
			<DoughnutChart
				criticals={criticals}
				deaths={deaths}
				recoveries={recoveries}
				/>
				</Col>
				<Col xs={12} md={6}>
					<div className="mapContainer" ref={mapContainerRef} />
				</Col>
			</Row>
		</React.Fragment>
	)
}

export async function getStaticProps() {  

  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
       }
  })

  const data = await res.json()
 
  return { 
    props: {
        data
    }
  }
}