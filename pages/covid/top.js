import { useState, useRef, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import toNum from "../../helpers/toNum";
import { Doughnut } from "react-chartjs-2";

import mapboxgl from "mapbox-gl";
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function top({ data }) {
  const countriesStats = data.countries_stat;
  const mapContainerRef = useRef(null);


  const countriesCases = countriesStats.map((countryStat) => {
    return {
      name: countryStat.country_name,
      cases: toNum(countryStat.cases),
    };
  });

  const countriesArray = countriesCases
    .sort((a, b) => {
      if (a.cases < b.cases) {
        return 1;
      } else if (a.cases > b.cases) {
        return -1;
      } else {
        return 0;
      }
    })
    .slice(0, 10);

  let countryNames = [];
  countriesArray.forEach((country) => {
    return countryNames.push(country.name);
  });

  let countryCases = [];
  countriesArray.forEach((country) => {
    return countryCases.push(country.cases);
  });

  //   useEffect(() => {
  //   if (countryNames.length); {
  //     countryNames.map((cn) => {
  //       fetch(
  //         `https://api.mapbox.com/geocoding/v5/mapbox.places/${cn}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`
  //       )
  //         .then((res) => res.json())
  //         .then((data) => {
  //           const marker = new mapboxgl.Marker()
  //           .setLngLat([data.features[0].center[0], data.features[0].center[1]])
  //           .addTo(map);
  //         });
  //     });
  //   }
  // }, [data]);

  // useEffect(() => {
  //   map = new mapboxgl.Map({
  //     container: mapContainerRef.current,

  //     style: "mapbox://styles/mapbox/streets-v11",
  //     center: [10, 10],
  //     zoom: [zoom],
  //   });

  //   map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
  //   return () => map.remove();
  // }, [data]);

    useEffect(() => {

          const map = new mapboxgl.Map({
          
            container: mapContainerRef.current,
          
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [44.63, 28.77],
            zoom: 0
        })

        map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

        for(let counter=0; counter < 10; counter++){

            fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${countryNames[counter]}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
            .then(res => res.json())
            .then(data => {
                  console.log(data)
                  
            const marker = new mapboxgl.Marker()
            .setLngLat([data.features[0].center[0], data.features[0].center[1]])
            .addTo(map)
            })
        }
        return () => map.remove()
      }, [])


  return (
    <React.Fragment>
      <h1> 10 Countries With the Highest Number of Cases</h1>
      <Doughnut
        data={{
          datasets: [
            {
              data: [...countryCases],
              backgroundColor: [
                "red",
                "yellow",
                "orange",
                "blue",
                "green",
                "indigo",
                "violet",
                "black",
                "gray",
                "white",
              ],
            },
          ],
          labels: [...countryNames],
        }}
        redraw={false}
      />
        <div className="mapContainer" ref={mapContainerRef} />
    </React.Fragment>
  );
}

export async function getStaticProps() {  

  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
       }
  })

  const data = await res.json()
 
  return { 
    props: {
        data
    }
  }
}