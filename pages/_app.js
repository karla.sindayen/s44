import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import 'mapbox-gl/dist/mapbox-gl.css'

import { Container } from 'react-bootstrap';
import NavBar from '../components/NavBar';

function MyApp({ Component, pageProps }) {
  return (
  	<React.Fragment>
  		<NavBar />
  		<Container>
  			<Component {...pageProps} />
  		</Container>
  	</React.Fragment>
  )
}

export default MyApp
