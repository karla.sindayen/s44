export default function toNum(str){
	//convert the string to an array to get access to array methods

	// 2,536,401
	//["2", ",","5","3","6",",","4","0","1"]
	const arr = [...str]

	//filter out the commas
	//["2", "5","3","6","4","0","1"]
	const filteredArr = arr.filter(element => element !== ",")

	//reduce the filtered array to a single strung without the commas
	//"2536401" => interger 2536401
	return parseInt(filteredArr.reduce((x, y) => x + y))
}
